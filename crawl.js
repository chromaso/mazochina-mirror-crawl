const assert = require('assert');
const Crawler = require('crawler');
const logger = require('./logger');
const { dateStrToTsInS } = require('./time_utils');
const { COOKIES, UA } = require('./conf');

const headers = {
    'Cookie': COOKIES,
    'User-Agent': UA
};

const crawler = new Crawler({
    retries: 0,
    logger: { log: () => { /* mute */ } },
    timeout: 20000
});

const assertTz = $ => {
    const timeZone = $('#nav-footer > li.rightside > span').text();
    assert.strictEqual(timeZone, 'UTC');
};

const maxPages = $ => {
    const pages = $('.action-bar.bar-bottom .pagination > ul li');
    if (!pages.length) {
        return 1;
    } else {
        const last = $(pages.get(pages.length - 1));
        if (!last.hasClass('next')) {
            return parseInt(last.text());
        } else { return parseInt($(pages.get(pages.length - 2)).text()); }
    }
};

module.exports.crawlIndexPage = () => new Promise((success, failed) => {
    crawler.direct({
        uri: 'http://forum.mazochina.com/index.php',
        headers,
        callback: (error, { statusCode, $ }) => {
            if (error) {
                logger.fail('index', 'transport', String(error));
                failed(error);
                return;
            }
            if (statusCode !== 200) {
                logger.fail('index', 'http', String(statusCode));
                failed(new RangeError('HTTP_' + statusCode));
                return;
            }
            assertTz($);
            const forums = $('.forabg ul.topiclist.forums > li');
            if (!forums.length) {
                logger.fail('index', 'content', $('.panel .inner').text().trim().replace(/\s+/g, ' '));
                failed();
            } else {
                logger.info('index', 'success', '');
                success({
                    forums: forums.toArray().map(el => {
                        const title = $('a.forumtitle', el);
                        assert(title.length === 1);
                        const href = title.attr('href');
                        if (href.match(/search\.php$/)) {
                            return null;
                        }
                        const matched = href.match(/viewforum\.php\?f=(\d+)$/);
                        assert(matched);
                        const forumId = parseInt(matched[1]);
                        const lastPost = $('dd.lastpost > span', el).contents().last().text();
                        return {
                            id: forumId,
                            lastPost: dateStrToTsInS(lastPost),
                        }
                    }).filter(_ => _)
                });
            }
        }
    });
});

module.exports.crawlForumPage = (forumId/* :int */, page/* :int */) => new Promise((success, failed) => {
    crawler.direct({
        uri: 'http://forum.mazochina.com/viewforum.php?f=' + String(forumId) + '&start=' + String((page - 1) * 30),
        headers,
        callback: (error, { statusCode, $ }) => {
            if (error) {
                logger.fail({ forumId, page }, 'transport', String(error));
                failed(error);
                return;
            }
            if (statusCode !== 200) {
                logger.fail({ forumId, page }, 'http', String(statusCode));
                failed(new RangeError('HTTP_' + statusCode));
                return;
            }
            assertTz($);
            const forumTitle = $('#page-body > h2.forum-title > a').text();
            const threads = $('.forumbg + .forumbg ul.topiclist.topics > li');
            const childForums = $('.forabg ul.topiclist.forums > li');
            if (!threads.length) {
                logger.info({ forumId, page }, 'content', $('.panel .inner').text().trim().replace(/\s+/g, ' '));
            }
            logger.info({ forumId, page }, 'success', forumTitle);
            success({
                threads: threads.toArray().map(el => {
                    const title = $('a.topictitle', el);
                    assert(title.length === 1);
                    const matched = title.attr('href').match(/viewtopic\.php\?f=(\d+)&t=(\d+)$/);
                    assert(matched);
                    const threadId = parseInt(matched[2]);
                    if (matched[1] !== String(forumId)) {
                        logger.fail({ forumId, threadId, page }, 'Unmatched forum ID', 'Moved');
                    }
                    const lastPost = $('dd.lastpost > span', el).contents().last().text();
                    let poster = $('.topic-poster a.username', el);
                    if (!poster.length) { poster = $('.topic-poster a.username-coloured', el); }
                    const authorIdMatched = poster.length ? poster.attr('href').match(/memberlist\.php\?mode=viewprofile&u=(\d+)$/) : null;
                    let firstPost = 0;
                    if (poster.length) {
                        const textContent = poster.parent().contents().last().text().trim();
                        if (textContent.charAt(0) === '»') {
                            firstPost = dateStrToTsInS(textContent.substr(1))
                        }
                    }
                    return {
                        title: title.text(),
                        id: threadId,
                        firstPost,
                        lastPost: dateStrToTsInS(lastPost),
                        sticky: !!($(el).hasClass('sticky')),
                        author: authorIdMatched ? parseInt(authorIdMatched[1]) : 0
                    };
                }),
                childForums: childForums.toArray().map(el => {
                    const title = $('a.forumtitle', el);
                    assert(title.length === 1);
                    const matched = title.attr('href').match(/viewforum\.php\?f=(\d+)$/);
                    assert(matched);
                    const forumId = parseInt(matched[1]);
                    const lastPost = $('dd.lastpost > span', el).contents().last().text();
                    return {
                        id: forumId,
                        lastPost: dateStrToTsInS(lastPost),
                    }
                }),
                page: maxPages($),
                title: forumTitle
            });
        }
    });
});

module.exports.crawlThreadPage = (forumId/* :int */, threadId/* :int */, page/* :int */) => new Promise((success, failed) => {
    if (page <= 0) { failed('WTF! Negative page!'); return; }
    crawler.direct({
        uri: 'http://forum.mazochina.com/viewtopic.php?f=' + String(forumId) + '&t=' + String(threadId) + '&start=' + String((page - 1) * 20),
        headers,
        callback: (error, { statusCode, $ }) => {
            if (error) {
                logger.fail({ threadId, page }, 'transport', String(error));
                failed(error);
                return;
            }
            if (statusCode === 404) {
                const testVal = $('.panel .inner').html().trim().replace(/\s+/g, ' ');
                if (testVal.indexOf('<p>The requested topic does not exist.</p>') > 0) {
                    logger.fail({ threadId, page }, 'non-existent', testVal);
                    failed(new RangeError('TOPIC_NOT_EXIST'));
                    return;
                }
            }
            if (statusCode === 403) {
                const testVal = $('.panel .inner').html().trim().replace(/\s+/g, ' ');
                if (testVal.indexOf('<p>You are not authorised to read this forum.</p>') > 0) {
                    logger.fail({ threadId, page }, 'not-authorised', testVal);
                    failed(new RangeError('TOPIC_NOT_AUTHORISED'));
                    return;
                }
            }
            if (statusCode !== 200) {
                logger.fail({ threadId, page }, 'http', String(statusCode));
                failed(new RangeError('HTTP_' + statusCode));
                return;
            }
            assertTz($);
            const threadTitle = $('#page-body > h2.topic-title > a').text();
            const posts = $('#page-body > .post > .inner > .postbody > div');
            const poll = $('#page-body > form.topic_poll');
            if (!posts.length) {
                logger.fail({ threadId, page }, 'content', testVal);
                failed();
            } else {
                logger.info({ threadId, page }, 'success', threadTitle);
                const breadCrumbForum = $('ul#nav-breadcrumbs li.breadcrumbs span.crumb:last-child a').attr('href');
                const matchedBc = breadCrumbForum.match(/viewforum\.php\?f=(\d+)$/);
                assert(matchedBc, 'breadCrumb link was ' + breadCrumbForum);
                success({
                    forumId: matchedBc[1],
                    posts: posts.toArray().map(el => {
                        const postIdMatched = $(el).attr('id').match(/^post_content(\d+)$/);
                        assert(postIdMatched);
                        let titleLine = $('h3 a', el).first();
                        if (!titleLine.length) { // Temporary fix as h3 is broken.
                            titleLine = $('a', $(el).children().get(0)).first();
                        }
                        const titleLinkMatched = titleLine.attr('href').match(/^#p(\d+)$/);
                        assert(titleLinkMatched && (postIdMatched[1] === titleLinkMatched[1]));
                        const authorLine = $('p.author', el);
                        assert(authorLine.length === 1);
                        let userId = 0;
                        let author = $('a.username', authorLine);
                        if (!author.length) { author = $('a.username-coloured', authorLine); }
                        if (author.length) {
                            const authorHref = author.attr('href');
                            assert(authorHref, `Cannot find author link in ${authorLine}`);
                            const authorIdMatched = authorHref.match(/memberlist\.php\?mode=viewprofile&u=(\d+)$/);
                            assert(authorIdMatched);
                            userId = parseInt(authorIdMatched[1]);
                        } else {
                            author = $('span.username', authorLine);
                            assert(author.length, 'No author row found...');
                        }
                        const date = authorLine.contents().last().text();
                        const contentEl = authorLine.next();
                        assert(contentEl.attr('class') === 'content');
                        const content = contentEl.html().replace(/&#x([0-9A-F]{1,4});/g, (original, quad) => String.fromCharCode(parseInt(quad, 16)));
                        const cites = $('blockquote div cite', contentEl);
                        cites.each((_, el) => $(el).parent().parent().remove());
                        return {
                            id: parseInt(postIdMatched[1]),
                            title: titleLine.text(),
                            userId,
                            username: author.text(),
                            date: dateStrToTsInS(date),
                            contentText: contentEl.text(),
                            content
                        }
                    }),
                    title: threadTitle,
                    page: maxPages($),
                    poll: poll.length ? $('h2.poll-title', poll[0]).text() : null
                });
            }
        }
    });
});

// module.exports.crawlIndexPage().then(console.log);
// module.exports.crawlForumPage(22, 1).then(console.log);
// module.exports.crawlThreadPage(18, 29311, 30).then(console.log).catch(console.warn);
if (require.main === module) {
    module.exports.crawlForumPage(22, 1).then(console.log).catch(console.warn);
}
