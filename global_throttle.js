const { Mutex } = require('async-mutex');
const { timeout } = require('./time_utils');
const { log: logger, fatalAndExit } = require('./logger');

let failures = 0;
let lastFinish = 0;
let serial = 0;

const mutex = new Mutex();

const log = (key, id) => {
    if (process.env['DEBUG']) {
        logger(key, id);
    }
};

// async
module.exports = async () => {
    const targetObject = {};
    Error.captureStackTrace(targetObject);
    const identifier = `M${++serial} [` + (targetObject.stack.split('\n')[2] || '').trim().substr(3) + ']';
    log(`mutex-wait`, identifier);

    const release = await mutex.acquire();
    const decayPrim = Math.min(failures, 6.4);
    const expectedDelay = Math.pow(3, decayPrim) * 50;
    const unfinished = expectedDelay - (Date.now() - lastFinish);
    if (unfinished > 50) {
        await timeout(unfinished);
    }
    let updated = false;

    log(`mutex-acquire`, identifier);
    return {
        fail: () => {
            if (updated) { fatalAndExit('Throttle updated twice'); return; }
            updated = true;
            failures++; lastFinish = Date.now();
            release();
            log(`mutex-fail-release`, identifier);
        },
        success: () => {
            if (updated) { fatalAndExit('Throttle updated twice'); return; }
            updated = true;
            failures = 0; lastFinish = Date.now();
            release();
            log(`mutex-success-release`, identifier);
        }
    };
};
