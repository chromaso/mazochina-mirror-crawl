const { threadQueue } = require('./queue');
const { query } = require('./db');
const { crawlThreadPage, crawlPostPage, crawlProfilePage } = require('./crawl.subs');
const throttle = require('./global_throttle');
const { now } = require('./time_utils');
const { toSc, containsCjk } = require('./chinese');
const { error, info } = require('./logger');
const { parseBBCode } = require('../serve/bbcode');
const assert = require('assert');

const rowsToMap = (rows, key) => {
    const retVal = new Map();
    for (const row of rows) {
        retVal.set(row[key], row);
    }
    return retVal;
};

const ensureUserExists = async postsArray => {
    if (!postsArray.length) { return; }
    const users = new Set((await query('SELECT user_id FROM users WHERE user_id = ANY($1::int[])', [postsArray.map(post => post.userId)])).rows.map(row => row.user_id));
    for (const post of postsArray) {
        if (!post.userId) {
            // TODO: create temp user automatically? maybe unneccessary.
            info({ postId: post.postId }, `skipping nonexistent user (${post.username})`);
            continue;
        } else if (!users.has(post.userId)) {
            info({ userId: post.userId }, `creating a user (${post.username})`);
            let registered = 0;
            try {
                registered = (await crawlProfilePage(post.userId)).registered;
            } catch (e) {
                error('user-crawl', { userId: post.userId, error: e.message });
            }
            await query('INSERT INTO users (user_id, user_name, registered_at) VALUES ($1, $2, $3) ON CONFLICT (user_id) DO NOTHING', [post.userId, post.username, registered]);
            users.add(post.userId);
        }
    }
};

// LINT.IfChange(special_sort_date_criteria)
const LENGTH_THRESHOLD_FOR_UPDATING_SORTPOST_DATE = 25;

const threadNeedsSpecialSortDateCalc = async (thread, txn) => {
    if ((thread.spam_likeness >= 40) || (thread.flags & 0x2)) { return true; }
     // Need to check is rep2see is enabled.
    const postFlags = (await txn('SELECT BIT_OR(flags) AS all_flags FROM posts WHERE thread_id = $1 AND posts.most_recent_update >= 0', [thread.thread_id])).rows[0];
    if (!postFlags) { console.log('Should never happen: no posts under thread ' + thread.thread_id); return true; }
    return !!(postFlags.all_flags & 0x2);
};
// LINT.ThenChange(../mazochina-mirror-serve/service_impl.ts:special_sort_date_criteria)

module.exports.doNext = async threadId => {
    const meta = (await query('SELECT forum_id, dirty_until_page, remote_most_recent_update, last_page_number, last_full_scan_requested FROM thread_targets WHERE thread_id = $1', [threadId])).rows[0];
    const pageToCrawl = meta.dirty_until_page + 1;
    const lock = await throttle();
    const refTime = now();
    let result;
    try {
        result = await crawlThreadPage(meta.forum_id, threadId, pageToCrawl);
    } catch (e) {
        if (e && e.message === 'TOPIC_NOT_EXIST') {
            lock.success();
            await query('UPDATE thread_targets SET successive_fail = -1000 WHERE thread_id = $1', [threadId]);
        } else if (e && e.message === 'TOPIC_NOT_AUTHORISED') {
            lock.success();
            await query('UPDATE thread_targets SET successive_fail = -2000 WHERE thread_id = $1', [threadId]);
        } else {
            lock.fail();
            await query('UPDATE thread_targets SET successive_fail = successive_fail + 1 WHERE thread_id = $1', [threadId]);
            error('thread-logic', e);
            // No need to push to threadQueue here. They'll be added to the queue in the next master loop anyway.
        }
        return;
    }
    lock.success();
    let threadRow = (await query('UPDATE thread_targets SET successive_fail = 0, last_success_crawl = $1, thread_title = $2, thread_title_sc = $3, remote_forum_id = $4, forum_id = COALESCE(NULLIF(forum_id, remote_forum_id), $4), poll_title = $5 WHERE thread_id = $6 RETURNING thread_id, flags, spam_likeness', [refTime, result.title, toSc(result.title), result.forumId, result.poll || '', threadId])).rows[0];
    if (pageToCrawl === 1) { // first page special
        let spamLikeness = 0;
        if (!containsCjk(result.title)) {
            spamLikeness += 15;
        }
        if (result.posts) {
            if (!containsCjk(result.posts[0].content)) {
                spamLikeness += 20;
            }
            if (result.posts[0].content.indexOf('<a href="http') >= 0) {
                spamLikeness += 15;
            }
            if (result.posts[0].content.indexOf('/?tg_id=') >= 0) {
                spamLikeness += 20 - Math.min(10, result.posts[0].content.length / 50);
            }
            if (result.posts[0].content.indexOf('已检测到广告，请尽快自行删除帖子') >= 0) {
                spamLikeness += 20;
            }
            const userInfo = (await query('SELECT flags FROM users WHERE user_name = $1', [result.posts[0].username])).rows[0];
            if (userInfo) {
                if (userInfo.flags & 2) { // Blocked.
                    spamLikeness += 35;
                } else if (userInfo.flags & 4) { // Verified.
                    spamLikeness = Math.max(0, spamLikeness - 25);
                }
            }
        }
        spamLikeness += 15 - 5 * Math.min(result.posts.length, 3);
        spamLikeness = Math.round(spamLikeness);
        if (spamLikeness === 40) { // to differentiate from manual soft-spam marking
            spamLikeness = 41;
        }
        const updatedRowOptional = (await query('UPDATE thread_targets SET spam_likeness = $1 WHERE thread_id = $2 AND spam_likeness > -1000 AND spam_likeness < 1001 AND spam_likeness <> 40 RETURNING thread_id, flags, spam_likeness', [spamLikeness, threadId])).rows[0];
        if (updatedRowOptional) {
            threadRow = updatedRowOptional;
        }
    }
    await ensureUserExists(result.posts);
    const threadNeedsSpecialSortDate = threadNeedsSpecialSortDateCalc(threadRow, query);
    let lastPostDateSatifyingWordCount = 0;
    const oldPosts = rowsToMap((await query('SELECT post_id, src_html FROM post_targets WHERE post_id = ANY($1::int[])', [result.posts.map(post => post.id)])).rows, 'post_id');
    for (const post of result.posts) {
        const oldPost = oldPosts.get(post.id);
        if (oldPost && (oldPost.src_html === post.content)) {
            // Content hasn't changed. Update the most recent date only.
            console.debug(`[Post ${post.id}] content hasn't changed.`);
            await query('UPDATE post_targets SET most_recent_recheck = $1 WHERE post_id = $2', [refTime, post.id]);
            await query('UPDATE posts SET most_recent_update = $1 WHERE post_id = $2 AND rev_id < 1073741824', [post.lastEdit || post.date, post.id]);
            continue;
        } 
        // Content has changed.
        let precisePostedDate = post.date;
        let bbCode;
        try {
            const postContent = await crawlPostPage(post.id);
            assert.strictEqual(postContent.userId, post.userId);
            precisePostedDate = postContent.postTime;
            bbCode = postContent.content;
        } catch (e) {
            // Should any errors exist, use the bare HTML directly.
        }

        const originalContent = post.content;
        let revIdForLogUse = 0;
        let revIdForPostUse = 0;
        if (bbCode) {
            const insertedRev = (await query('INSERT INTO local_post_log (rev_id, post_id, updated_at, title, content, flags) VALUES (nextval(\'remote_rev_id\'), $1, $2, $3, $4, 0) RETURNING rev_id', [post.id, post.lastEdit || precisePostedDate, post.title, bbCode])).rows[0];
            revIdForLogUse = insertedRev.rev_id;
            try {
                const parsed = parseBBCode(bbCode, false, true);
                post.content = parsed.html;
                post.contentText = parsed.text;
                revIdForPostUse = insertedRev.rev_id;
            } catch (e) {
                error('bbcode-parsing', { e: e.message, post: post.id });
                // Do nothing.
            }
        }

        console.debug(`[Post ${post.id}] content has changed rev_id = ${revIdForPostUse} (${revIdForLogUse}).`);
        if ((!threadNeedsSpecialSortDate) || (post.contentText.replace(/\s+/g, '').length >= LENGTH_THRESHOLD_FOR_UPDATING_SORTPOST_DATE)) {
            lastPostDateSatifyingWordCount = post.date;
        }

        await query('INSERT INTO post_targets (post_id, crawled_at, src_html, done_as_rev, most_recent_recheck) VALUES ($1, $2, $3, $4, $2) ON CONFLICT (post_id) DO UPDATE SET crawled_at = EXCLUDED.crawled_at, src_html = EXCLUDED.src_html, done_as_rev = EXCLUDED.done_as_rev, most_recent_recheck = EXCLUDED.most_recent_recheck', [post.id, refTime, originalContent, revIdForLogUse]);

        await query('INSERT INTO posts (post_id, thread_id, title, user_name, content, posted_date, most_recent_update, content_sc, rev_id, user_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) ON CONFLICT (post_id) DO ' + (post.content.trim().length ? 'UPDATE SET thread_id = EXCLUDED.thread_id, title = EXCLUDED.title, user_name = EXCLUDED.user_name, content = EXCLUDED.content, posted_date = EXCLUDED.posted_date, most_recent_update = EXCLUDED.most_recent_update, content_sc = EXCLUDED.content_sc, rev_id = EXCLUDED.rev_id, user_id = EXCLUDED.user_id WHERE posts.rev_id < 1073741824' : 'NOTHING'), [post.id, threadId, post.title, post.username, post.content, precisePostedDate, post.lastEdit || precisePostedDate, toSc(post.title + '\n\ufffc\n' + post.contentText), revIdForPostUse, post.userId]);
    }

    const hoursSinceFullScan = Math.round((refTime - meta.last_full_scan_requested) / 3600);
    if (result.page <= pageToCrawl) { // already last page
        await query('UPDATE thread_targets SET remote_most_recent_update = $1, lastpost_date = GREATEST(lastpost_date, $1), sortpost_date = GREATEST(sortpost_date, $4), dirty_most_recent_update = 0, dirty_until_page = 0, last_page_number = $2 WHERE thread_id = $3', [result.posts[result.posts.length - 1].date, result.page, threadId, lastPostDateSatifyingWordCount]);
        if ((hoursSinceFullScan < 2) || (result.page === 1)) {
            await query('UPDATE posts SET most_recent_update = -ABS(most_recent_update) WHERE thread_id = $1 AND COALESCE((SELECT most_recent_recheck FROM post_targets WHERE post_id = posts.post_id), $2) < $2 AND post_id < 268435456', [threadId, refTime - ((result.page === 1) ? 300 : 7200)]); // if a post is not seen for more than 5 minutes / 2 hours, it's likely to have been deleted
        }
        return;
    }
    let nextPage = pageToCrawl; // go to next page by default
    if ((pageToCrawl === 1) && (meta.last_page_number > 1)) {
        if (hoursSinceFullScan >= 2 * 7 * 24) { // 2 weeks
            info({ threadId }, 'full-scan requested', `${hoursSinceFullScan} hrs since last`);
            await query('UPDATE thread_targets SET last_full_scan_requested = $1 WHERE thread_id = $2', [refTime, threadId]);
        } else {
            info({ threadId }, 'full-scan skipped', `${hoursSinceFullScan} hrs since last`);
            nextPage = meta.last_page_number - 1; // skip unchanged pages, head to hot area
        }
    } else if ((result.posts[0].date > meta.remote_most_recent_update) && (pageToCrawl <= meta.last_page_number)) { // only happens when some post are deleted, so that the following posts moved to previous pages
        nextPage = Math.max(0, pageToCrawl - 2);
        await query('UPDATE thread_targets SET last_page_number = $1 WHERE thread_id = $2', [meta.last_page_number - 1, threadId]);
    }
    await query('UPDATE thread_targets SET dirty_until_page = $1 WHERE thread_id = $2', [nextPage, threadId]);
    threadQueue.push(threadId);
};
