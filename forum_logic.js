const { forumQueue } = require('./queue');
const { query } = require('./db');
const { crawlForumPage } = require('./crawl');
const throttle = require('./global_throttle');
const { now } = require('./time_utils');
const { toSc } = require('./chinese');

const propagateMostRecentUpdateUpwards = async forumId => {
    const parentForumId = (await query('UPDATE forum_targets tgt SET most_recent_update_include_children = GREATEST(children.updated, self.most_recent_update) FROM (SELECT COALESCE(MAX(most_recent_update_include_children), 0) AS updated FROM forum_targets WHERE parent_forum_id = $1) children, (SELECT most_recent_update FROM forum_targets WHERE forum_id = $1) self WHERE tgt.forum_id = $1 RETURNING tgt.parent_forum_id', [forumId])).rows[0].parent_forum_id;
    if (parentForumId) {
        await propagateMostRecentUpdateUpwards(parentForumId);
    }
};

module.exports.doNext = async forumId => {
    const meta = (await query('SELECT dirty_until_page, most_recent_update, parent_forum_id FROM forum_targets WHERE forum_id = $1', [forumId])).rows[0];
    const pageToCrawl = meta.dirty_until_page + 1;
    const lock = await throttle();
    let result;
    try {
        result = await crawlForumPage(forumId, pageToCrawl);
    } catch (e) {
        lock.fail();
        await query('UPDATE forum_targets SET successive_fail = successive_fail + 1 WHERE forum_id = $1', [forumId]);
        forumQueue.push(forumId);
        return;
    }
    lock.success();
    const refTime = now();
    const queries = [];
    queries.push(query('UPDATE forum_targets SET successive_fail = 0, last_success_crawl = $1, forum_name = $2, dirty_until_page = $3 WHERE forum_id = $4', [refTime, result.title, pageToCrawl, forumId]));
    if (result.threads.length) {
        if (pageToCrawl === 1) {
            const lastPost = result.threads.find(th => !th.sticky).lastPost;
            queries.push(query('UPDATE forum_targets SET dirty_most_recent_update = $1 WHERE forum_id = $2', [lastPost, forumId]));
        }
        result.threads.forEach(thread => {
            queries.push(query('INSERT INTO thread_targets AS old_th (thread_id, forum_id, remote_forum_id, thread_title, thread_title_sc, firstpost_date, dirty_most_recent_update, owner_user_id) VALUES ($1, $2, $2, $3, $4, $5, $6, $7) ON CONFLICT (thread_id) DO UPDATE SET thread_title = EXCLUDED.thread_title, thread_title_sc = EXCLUDED.thread_title_sc, dirty_most_recent_update = COALESCE(NULLIF(EXCLUDED.dirty_most_recent_update, old_th.remote_most_recent_update), 0), successive_fail = GREATEST(0, old_th.successive_fail)', [thread.id, forumId, thread.title, toSc(thread.title), thread.firstPost, thread.lastPost, thread.author]));
        });
        result.childForums.forEach(childForum => {
            queries.push(query('UPDATE forum_targets SET dirty_most_recent_update = COALESCE(NULLIF($1, most_recent_update_include_children), 0) WHERE dirty_most_recent_update = 0 AND forum_id = $2', [childForum.lastPost, childForum.id]));
        });
        await Promise.all(queries); // make sure previous SQLs are done before reading dirty_most_recent_update and changing dirty_until_page again
    }
    if ((result.page === pageToCrawl) /* last page already */ || (!result.threads.length) || (result.threads[result.threads.length - 1].lastPost < meta.most_recent_update) /* all new post cleared */) {
        await query('UPDATE forum_targets SET most_recent_update = GREATEST(dirty_most_recent_update, most_recent_update), dirty_most_recent_update = 0, dirty_until_page = 0 WHERE forum_id = $1', [forumId]);
        await propagateMostRecentUpdateUpwards(forumId, meta.parent_forum_id);
    } else {
        forumQueue.push(forumId);
    }
};
