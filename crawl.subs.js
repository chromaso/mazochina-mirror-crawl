'use strict';

const assert = require('assert');
const Crawler = require('crawler');
const { SS_COOKIES, UA } = require('./conf');
const logger = require('./logger');
const { dateStrToTsInS } = require('./time_utils');

const headers = {
    'Cookie': SS_COOKIES,
    'User-Agent': UA
};

const crawler = new Crawler({
    retries: 0,
    logger: { log: () => { /* mute */ } },
    timeout: 20000
});

const assertTz = $ => {
    const timeZone = $('p.datetime span[title]').first().text().trim();
    assert.strictEqual(timeZone, 'UTC');
};

const maxPages = $ => {
    const overview = $('#pagecontent td.nav').first().text().trim().replace(/\s+/g, ' ');
    const match = overview.match(/^Page \d+ of (\d+)$/);
    assert(match);
    return parseInt(match[1]);
};

module.exports.crawlThreadPage = (forumId/* :int */, threadId/* :int */, page/* :int */) => new Promise((success, failed) => {
    if (page <= 0) { failed('WTF! Negative page!'); return; }
    crawler.direct({
        uri: 'http://forum.mazochina.com/viewtopic.php?f=' + String(forumId) + '&t=' + String(threadId) + '&start=' + String((page - 1) * 20),
        headers,
        callback: (error, { statusCode, $ }) => {
            if (error) {
                logger.fail({ threadId, page }, 'transport', String(error));
                failed(error);
                return;
            }
            if (statusCode === 404) {
                const testVal = $('p.gen').text().trim().replace(/\s+/g, ' ');
                if (testVal.includes('The requested topic does not exist.')) {
                    failed(new RangeError('TOPIC_NOT_EXIST'));
                    return;
                }
            }
            if (statusCode === 403) {
                const testVal = $('p.gen').text().trim().replace(/\s+/g, ' ');
                if (testVal.includes('>You are not authorised to read this forum.')) {
                    failed(new RangeError('TOPIC_NOT_AUTHORISED'));
                    return;
                }
            }
            if (statusCode !== 200) {
                logger.fail({ threadId, page }, 'http', String(statusCode));
                failed(new RangeError('HTTP_' + statusCode));
                return;
            }
            assertTz($);
            const threadTitle = $('#pageheader > h2 > a').text();
            const postEls = $('#pagecontent > table.tablebg > tr.row1:nth-child(2) + tr.row1, #pagecontent > table.tablebg > tr.row2:nth-child(2) + tr.row2');
            const poll = $('#pagecontent form[action^="./viewtopic.php"]').toArray().find(candidate => $('input[name="vote_id[]"]', candidate).length);
            if (!postEls.length) {
                logger.fail({ threadId, page }, 'content', 'no post found');
                failed();
            } else {
                logger.info({ threadId, page }, 'success', threadTitle);
                const breadCrumbForum = $('span[data-forum-id]:last-child', $('p.breadcrumbs')[0]);
                assert(breadCrumbForum, 'breadcrumb not found');
                const posts = postEls.toArray().map(elem => {
                    const ts = $(elem.parentNode).children('tr.row1, tr.row2');
                    assert.strictEqual(ts.length, 3);
                    const headTable = $($(ts[0]).children()[1]).children('table')[0];
                    assert.strictEqual(headTable.tagName.toLowerCase(), 'table');

                    const authorName = $('b.postauthor', $(ts[0]).children()[0]);
                    const postTitle = $('td.gensmall > div:first-child b + a[href^="#p"]', headTable);
                    const postDateSib = $('td.gensmall > div:last-child b', headTable)[0];
                    const postBody = $('.postbody:first-child', ts[1]).first();
                    const lastEditCandidates = $('.postbody ~ span.gensmall', ts[1]);
                    const footerProfileLink =  $('td.profile + td > .gensmall > a.imageset[href^="./memberlist.php"]', ts[2]);

                    const postIdMatched = postTitle.attr('href').match(/^#p(\d+)$/);
                    assert(postIdMatched);
                    let userId = 0;
                    if (footerProfileLink.length) {
                        const authorIdMatched = footerProfileLink.attr('href').match(/memberlist\.php\?mode=viewprofile&u=(\d+)$/);
                        assert(authorIdMatched);
                        userId = parseInt(authorIdMatched[1]);
                    }
                    const expectedDate = postDateSib.nextSibling;
                    assert.strictEqual(expectedDate.type, 'text');
                    const date = (expectedDate.data);

                    $('.quotetitle', postBody).each((_, el) => $(el).remove());
                    const quoteContents = $('.quotecontent', postBody);
                    quoteContents.each((_, el) => { el.tagName = 'blockquote'; });

                    const content = postBody.html().replace(/&#x([0-9A-F]{1,4});/g, (original, quad) => String.fromCharCode(parseInt(quad, 16)));
                    quoteContents.each((_, el) => $(el).remove());

                    let lastEdit = 0;
                    lastEditCandidates.each((_, el) => {
                        const text = $(el).text().trim();
                        if (text.startsWith('Last edited by ')) {
                            const matched = text.match(/on (.*\:.*), edited \d+ times? in total/);
                            assert(matched);
                            lastEdit = dateStrToTsInS(matched[1]);
                        }
                    });
                    return {
                        id: parseInt(postIdMatched[1]),
                        title: postTitle.text(),
                        userId,
                        username: authorName.text(),
                        date: dateStrToTsInS(date),
                        lastEdit,
                        contentText: postBody.text(),
                        content
                    }
                });
                success({
                    forumId: parseInt(breadCrumbForum.attr('data-forum-id')),
                    posts: posts,
                    title: threadTitle,
                    page: maxPages($),
                    poll: poll ? $('span.gen b', poll).text() : null
                });
            }
        }
    });
});

module.exports.crawlPostPage = (postId/* :int */) => new Promise((success, failed) => {
    crawler.direct({
        uri: 'http://forum.mazochina.com/posting.php?mode=quote&f=52&p=' + postId,
        headers,
        callback: (error, { statusCode, $ }) => {
            if (error) {
                failed(error);
                return;
            }
            if (statusCode !== 200) {
                failed(new RangeError('HTTP_' + statusCode));
                return;
            }
            const testVal = $('p.gen').text().trim().replace(/\s+/g, ' ');
            if (testVal.includes('The requested post does not exist.')) {
                failed(new RangeError('POST_NOT_EXIST'));
                return;
            }
            const nonLogined = $('.login-title').text().trim().replace();
            if (nonLogined.includes('You need to login in order to post within this forum.')) {
                failed(new RangeError('LOGIN_EXPIRED'));
                return;
            }
            assertTz($);
            const textarea = $('textarea[name=message]');
            if (textarea.length !== 1) { 
                failed(new RangeError('TEXTAREA_MISSING'));
                return;
            }
            const content = textarea.val().trim();
            if (!content) {
                failed(new RangeError('TEXTAREA_EMPTY'));
                return;
            }
            const match = content.match(/^\[quote=([^ \]]+) post_id=(\d+) time=(\d+) user_id=(\d+)\]([\s\S]*)\[\/quote\]$/);
            if (!match) {
                failed(new RangeError('TEXTAREA_UNPARSEABLE'));
                return;
            }
            assert.strictEqual(parseInt(match[2]), postId);
            success({
                userName: match[1],
                postTime: parseInt(match[3]),
                userId: parseInt(match[4]),
                content: match[5].trim()
            });
        }
    });
});

module.exports.crawlProfilePage = (userId/* :int */) => new Promise((success, failed) => {
    crawler.direct({
        uri: 'http://forum.mazochina.com/memberlist.php?mode=viewprofile&u=' + userId,
        headers,
        callback: (error, { statusCode, $ }) => {
            if (error) {
                failed(error);
                return;
            }
            if (statusCode !== 200) {
                failed(new RangeError('HTTP_' + statusCode));
                return;
            }
            const testVal = $('p.gen').text().trim().replace(/\s+/g, ' ');
            if (testVal.includes('The requested user does not exist.')) {
                failed(new RangeError('USER_NOT_EXIST'));
                return;
            }
            const nonLogined = $('.login-title').text().trim().replace();
            if (nonLogined.includes('The board requires you to be registered and logged in to view profiles.')) {
                failed(new RangeError('LOGIN_EXPIRED'));
                return;
            }
            assertTz($);
            const allRows = $('#pagecontent table.tablebg > tr > td > table > tr');
            const meta = new Map();
            allRows.each((_, row) => {
                const td = $(row).children('td');
                if (td.length !== 2) { return; }
                const text0 = td.first().text().trim();
                const text1 = td.last().text().trim();
                meta.set(text0, text1);
            });
            const date = meta.get('Joined:');
            if (!date) {
                failed(new RangeError('DATE_NOT_FOUND'));
                return;
            }
            success({
                registered: dateStrToTsInS(date)
            });
        }
    });
});

// module.exports.crawlIndexPage().then(console.log);
// module.exports.crawlForumPage(22, 1).then(console.log);
// module.exports.crawlThreadPage(18, 29311, 30).then(console.log).catch(console.warn);
if (require.main === module) {
    module.exports.crawlProfilePage(25646).then(console.log).catch(console.warn);
    // module.exports.crawlPostPage(290070).then(console.log).catch(console.warn);
}
