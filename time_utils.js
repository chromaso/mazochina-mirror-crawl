module.exports.now = () => Math.floor(Date.now() / 1000);

module.exports.timeout = timeoutMs => new Promise(res => setTimeout(res, timeoutMs));

module.exports.dateStrToTsInS = date => Math.floor(Date.parse(date.trim() + ' UTC') / 1000);