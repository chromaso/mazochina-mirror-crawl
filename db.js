const { Pool } = require('pg');
const { DB_HOST, DB_PASSWORD } = require('./conf');

const pool = new Pool({
    user: 'mazomirror',
    host: DB_HOST,
    database: 'mazomirror',
    password: DB_PASSWORD
});

module.exports.query = (sql, values) => pool.query(sql, values);
