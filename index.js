const { forumQueue, threadQueue } = require('./queue');
const { timeout, now } = require('./time_utils');
const logger = require('./logger');
const { query } = require('./db');

const indexLogic = require('./index_logic');
const forumLogic = require('./forum_logic');
const threadLogic = require('./thread_logic');

require('os').setPriority(0, 15);

const indexLoopReinitiate = async () => {
    while (true) {
        try {
            await indexLogic.doNext();
            await timeout(120 * 1000); // 2 minutes per refresh
        } catch (e) {
            logger.error('index', e);
            await timeout(15 * 1000); // 15-sec additional delay when failed
        }
    }
};

const forumQueueReinitiate = async () => {
    while (true) {
        const res = await query('SELECT forum_id FROM forum_targets WHERE forum_id < 1073741824 AND dirty_most_recent_update > 0'); // refresh_interval should be set long enough, it is only used for forced-refreshes. normal refreshed will come from index_logic
        res.rows.forEach(row => {
            forumQueue.push(row.forum_id);
        });
        if (res.rows.length <= 1) { // add a hard-refresh
            (await query('SELECT forum_id FROM forum_targets WHERE forum_id < 1073741824 AND (last_success_crawl + refresh_interval < $1) LIMIT 1', [now()])).rows.forEach(row => {
                forumQueue.pushLow(row.forum_id);
            })
        }
        let forumId;
        while (forumId = forumQueue.pop()) {
            await forumLogic.doNext(forumId);
        }
        await timeout(25 * 1000);
    }
};

const threadQueueReinitiate = async () => {
    while (true) {
        const res = await query('SELECT thread_id FROM thread_targets WHERE thread_id < 268435456 AND dirty_most_recent_update <> 0 AND successive_fail >= 0 LIMIT 50');
        res.rows.forEach(row => {
            threadQueue.push(row.thread_id);
        });
        if (res.rows.length <= 1) { // add a re-visit task
            (await query('SELECT thread_id FROM thread_targets WHERE thread_id < 268435456 AND last_success_crawl > 0 AND remote_most_recent_update > 0 AND last_success_crawl < $1 AND successive_fail >= 0 ORDER BY (remote_most_recent_update / 4 - last_success_crawl) DESC LIMIT 1', [now() - 2 * 7 * 86400])).rows.forEach(row => {
                threadQueue.pushLow(row.thread_id);
            });
        }
        let threadId;
        while (threadId = threadQueue.pop()) {
            await threadLogic.doNext(threadId);
        }
        await timeout(25 * 1000);
    }
};

Promise.all([indexLoopReinitiate(), forumQueueReinitiate(), threadQueueReinitiate()]).catch(e => {
    logger.fatalAndExit(e);
});

setTimeout(() => { process.exit(-1); }, 12 * 60 * 60 * 1000);
